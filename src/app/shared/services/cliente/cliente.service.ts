import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { Cliente } from '../../models/cliente';


@Injectable()
export class ClienteService {

    constructor(private http: HttpClient) { }
    
    getClientes() {
        const promise = new Promise((resolve, reject) => {
            this.http
                .get<Cliente[]>('https://localhost:5001/api/ClienteDW')
                .toPromise()
                .then((res: any) => {
                    resolve(res.map(p => { return <Cliente[]>p }));
                },
                    err => {
                        reject(err);
                    }
                );
        });
        return promise;
    }
}