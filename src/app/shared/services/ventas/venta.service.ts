import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { CrearVenta, EditarDetalleVentaProducto, EditarVenta, ListaDetalleVentaProducto, Venta, VentaLista } from '../../models/venta';


@Injectable()
export class VentaService{

    constructor(private http: HttpClient) { }

   

    //deprecated metodo por no usar promises
    obtenerVentas(): Observable<Venta[]> {
        console.log('obtener todas las ventas');
        return this.http.get<Venta[]>('https://localhost:5001/api/venta/ObtenerVentas');
    }
     
    //deprecated metodo por no usar promises
    crearVenta(nuevaVenta: Venta): Observable<Venta> {
        return this.http.post<Venta>('https://localhost:5001/api/venta/Crear', nuevaVenta, {
            headers: new HttpHeaders({
                'Content-Type': 'application/json'
            })
        });
    }
    //Deprecated metodo por no usar promises
    editarVentaVersionDos(editarVenta: EditarVenta, id: number): Observable<void> {
        return this.http.put<void>(`https://localhost:5001/api/VentaDW/${id}`, editarVenta, {
            headers: new HttpHeaders({
                'Content-Type': 'application/json'
            })
        });
    }
    
    //Deprecated metodo por no usar promises
    editarVenta(editarVenta: EditarVenta ): Observable<void> {
        return this.http.put<void>('https://localhost:5001/api/venta/Editar/', editarVenta, {
            headers: new HttpHeaders({
                'Content-Type': 'application/json'
            })
        });
    }

    obtenerListaVentas() {
        const promise = new Promise((resolve, reject) => {
            this.http
                .get<VentaLista[]>('https://localhost:5001/api/VentaDW')
                .toPromise()
                .then((res: any) => {
                    resolve(res.map(p => { return <VentaLista[]>p }));
                },
                    err => {
                        reject(err);
                    }
                );
        });
        return promise;
    }

    editarVentaPromise(editarVenta: EditarVenta, id: number) {
        const promise = new Promise((resolve, reject) => {
            this.http
                .put<void>(`https://localhost:5001/api/VentaDW/${id}`, editarVenta, {
                    headers: new HttpHeaders({
                        'Content-Type': 'application/json'
                    })
                })
                .toPromise()
                .then((res: any) => {
                    resolve(true);
                },
                    err => {
                        reject(err);
                    }
                );
        });
        return promise;
    }

    editarVentaDetallePromise(editarDetalleVentaProducto: EditarDetalleVentaProducto, ventaId: number, id: number) {
        const promise = new Promise((resolve, reject) => {
            this.http
                .put<void>(`https://localhost:5001/api/VentaDW/${ventaId}/DetalleVentaProductoDW/${id}`, editarDetalleVentaProducto, {
                    headers: new HttpHeaders({
                        'Content-Type': 'application/json'
                    })
                })
                .toPromise()
                .then((res: any) => {
                    resolve(true);
                },
                    err => {
                        reject(err);
                    }
                );
        });
        return promise;
    }

    crearVentaPromise(crearVenta: CrearVenta) {
        const promise = new Promise((resolve, reject) => {
            this.http
                .post<VentaLista>('https://localhost:5001/api/VentaDW/', crearVenta, {
                    headers: new HttpHeaders({
                        'Content-Type': 'application/json'
                    })
                })
                .toPromise()
                .then((respuesta: VentaLista) => {
                    resolve(respuesta);
                },
                    err => {
                        reject(err);
                    }
                );
        });
        return promise;
    }

    crearVentaDetallePromise(crearDetalleVentaProducto: ListaDetalleVentaProducto, ventaId: number) {
        const promise = new Promise((resolve, reject) => {
            this.http
                .post<void>(`https://localhost:5001/api/VentaDW/${ventaId}/DetalleVentaProductoDW`, crearDetalleVentaProducto, {
                    headers: new HttpHeaders({
                        'Content-Type': 'application/json'
                    })
                })
                .toPromise()
                .then((res: any) => {
                    resolve(true);
                },
                    err => {
                        reject(err);
                    }
                );
        });
        return promise;
    }

    //deprecated
    borrarVenta(ventaID: number): Observable<void> {
        return this.http.delete<void>(`https://localhost:5001/api/venta/Borrar/${ventaID}`);
    }

}