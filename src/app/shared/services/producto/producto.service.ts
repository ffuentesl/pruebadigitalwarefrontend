import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { Venta } from '../../models/venta';
import { Producto, ProductoError, ProductoLista, ProductosLista } from '../../models/producto';


@Injectable()
export class ProductoService {

    constructor(private http: HttpClient) { }
    lista:ProductoLista[];

    // deprecated
    obtenerListaProductos(): Observable<ProductoLista[] | ProductoError> {
        console.log('obtener todas las vent');
        return this.http.get<ProductoLista[]>('https://localhost:5001/api/ProductoDW')
        .pipe(
            map(p => { return <ProductoLista[]>p }),
            catchError(error=> this.handleHttpError(error))
        );
    }

    getProductos() {
        const promise = new Promise((resolve, reject) => {
            this.http
                .get<ProductosLista[]>('https://localhost:5001/api/ProductoDW')
                .toPromise()
                .then((res: any) => { 
                    resolve(res.map(p => { return <ProductosLista[]>p }));
                },
                    err => { 
                        reject(err);
                    }
                );
        });
        return promise;
    }

    //deprecated
    private handleHttpError(error: HttpErrorResponse): Observable<ProductoError> {
        let dataError = new ProductoError();
        dataError.numeroError = 100;
        dataError.mensage = error.statusText;
        dataError.mensageSecundario = 'error al traer los datos';
        return throwError(dataError);
    }
    //deprecated
    crearProducto(nuevaProducto: Producto): Observable<Producto> {
        return this.http.post<Producto>('https://localhost:5001/api/ProductoDW', nuevaProducto, {
            headers: new HttpHeaders({
                'Content-Type': 'application/json'
            })
        });
    }
}