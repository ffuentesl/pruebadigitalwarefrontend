import { Producto } from "./producto";
import { Venta } from "./venta";

 export  class DetalleVentaProducto {
     id: number;
     venta?: Venta;
     ventaId: number;
     productoId:number;
     producto?: Producto;

     constructor(id: number, venta: Venta, ventaId: number, productoId: number, producto: Producto){
         this.id = id;
         this.venta = venta;
         this.ventaId = ventaId;
         this.productoId =productoId;
         this.producto = producto;
     }
}
