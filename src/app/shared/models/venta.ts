import { Cliente } from "./cliente";
import { DetalleVentaProducto } from "./detalleVentaProducto";

export class Venta {
    id: number;
    fehcaVenta: string;
    clienteId: number;
    cliente?: Cliente;
    listaDetalleVentaProducto: DetalleVentaProducto[];
 
    constructor(id: number,
        fehcaVenta: string,
        clienteId: number,
        cliente: Cliente,
        listaDetalleVentaProducto: DetalleVentaProducto[]){
        this.id = id;
        this.fehcaVenta = fehcaVenta;
        this.clienteId = clienteId;
        this.cliente = cliente ;
        this.listaDetalleVentaProducto = listaDetalleVentaProducto;
        }
    
}

export class borrarVenta{
    BorrarId:number;
}


export class VentaLista {
    id: number;
    fehcaVenta: string;
    cliente: Cliente;
    clienteId: number;
    listaDetalleVentaProducto: ListaDetalleVentaProducto[];
} 

export class ListaDetalleVentaProducto {
    id: number;
    producto: Producto;
    productoId: number;
    existenciaProductoId: number;
}

export class Producto {
    id: number;
    nombre: string;
    referencia: string;
    tipoProducto: number;
    fechaIngreso: Date;
    listaExistenciaDetalleProductos: any[];
}
 
export class EditarVenta {
    fehcaVenta: string;
    clienteId: number;

    constructor(fehcaVenta: string, clienteId: number){
          this.fehcaVenta=  fehcaVenta;
          this.clienteId = clienteId
    }
}

export class EditarDetalleVentaProducto {
    existenciaProductoId: number;
    productoId: number;

    constructor(existenciaProductoId: number, productoId: number) {
        this.existenciaProductoId = existenciaProductoId;
        this.productoId = productoId
    }
}
 
export class CrearVenta {
    fehcaVenta: string;
    clienteId: number;
    cliente: Cliente;
    constructor(fehcaVenta: string, clienteId: number, cliente: Cliente) {
        this.fehcaVenta = fehcaVenta;
        this.clienteId = clienteId;
        this.cliente = cliente;
    }
}
 