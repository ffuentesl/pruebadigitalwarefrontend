import { ExistenciaProducto } from "./existenciaProducto";

export class Producto{
    nombre:string;
    referencia:string;
    tipoProducto: number;
    fechaIngreso: Date;
    ListaExistenciaDetalleProductos: [ExistenciaProducto];
}

export class ProductoLista {
    id: number;
    nombre: string;
    referencia: string;
    tipoProducto: number;
    // fechaIngreso: Date;
    listaExistenciaDetalleProductos: ListaExistenciaDetalleProducto[];
}
export class ProductosLista {
    id: number;
    nombre: string;
    referencia: string;
    tipoProducto: number;
     fechaIngreso: Date;
    listaExistenciaDetalleProductos: ListaExistenciaDetalleProducto[];
    numeroProductos:number;
    constructor(id: number, nombre: string, referencia: string, tipoProducto: number,
        fechaIngreso: Date, listaExistenciaDetalleProductos: ListaExistenciaDetalleProducto[], numeroProductos:number)
        {
            this.id=id;
            this.nombre=nombre;
            this.referencia=referencia;
            this.tipoProducto=tipoProducto;
            this.fechaIngreso=fechaIngreso;
            this.listaExistenciaDetalleProductos= listaExistenciaDetalleProductos;
            this.numeroProductos = numeroProductos;          
        }

}
export class ListaExistenciaDetalleProducto {
    id: number;
    vendido: boolean;
    estadoProducto: number;
    serie: string;
}

export class ProductoError {
    numeroError: number;
    mensage: string;
    mensageSecundario: string;
}