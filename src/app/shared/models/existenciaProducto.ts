import { Producto } from "./producto";

export class ExistenciaProducto{
    id: number;
    vendido:boolean;
    serie:string;
    producto: [Producto];
    productoId:number;
}