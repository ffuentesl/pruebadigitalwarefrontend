export class Cliente{
    id: number;
    nombre:string;
    apellido:string;
    telefonoCelularFijo:string;
    identificacion:number;
    fechaNacimiento:string;
}