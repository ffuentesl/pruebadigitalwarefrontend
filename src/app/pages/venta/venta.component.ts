import { Component, OnInit, ViewChild } from '@angular/core';

import ArrayStore from 'devextreme/data/array_store';
import { Product, Service } from 'src/app/shared/services/pruebaSelect/product.service';
import { ProductUp, ServiceUp } from 'src/app/shared/services/pruebaSelect/productUp.service';

import DataSource from 'devextreme/data/data_source';
import { ProductoService } from 'src/app/shared/services/producto/producto.service';
import { ProductoLista } from 'src/app/shared/models/producto';
import { Input } from '@angular/core';
import { Employee, ServiceDGE, State } from 'src/app/shared/services/pruebaSelect/dataGridModule';
import { VentaService } from 'src/app/shared/services/ventas/venta.service';
import { CrearVenta, EditarDetalleVentaProducto, EditarVenta, ListaDetalleVentaProducto, VentaLista } from 'src/app/shared/models/venta';
import { DxDataGridComponent } from 'devextreme-angular';
import notify from 'devextreme/ui/notify';
import { ClienteService } from 'src/app/shared/services/cliente/cliente.service';
import { Cliente } from 'src/app/shared/models/cliente';


@Component({
  selector: 'app-venta',
  templateUrl: './venta.component.html',
  styleUrls: ['./venta.component.scss'],
  providers: [Service, ServiceUp, ServiceDGE, ProductoService, VentaService, ClienteService]
}) 
export class VentaComponent implements OnInit {

  // constructor() { }
  simpleProducts: string[];
  products: Product[];
  data: any;

  fromUngroupedData: DataSource;
  fromPregroupedData: DataSource;

  //Select Box Agrupado
  listaProductos: DataSource;
  productosLista: ProductoLista[];
  pro: ProductoLista;
  @Input() public productoKe: any;



  //DataGridModule ventas
  dataSourceVentas: VentaLista[];
  selectVentaLista: VentaLista;
  selectVentaListaProducto: ListaDetalleVentaProducto[];

  // crearVenta
  crearVenta: CrearVenta = new CrearVenta("", 0, new Cliente());
  crearVentaProducto: VentaLista = new VentaLista();
  crearVentaListaProducto: ListaDetalleVentaProducto[] = [];
  popupCrearVenta = false;

  //ListaClientes
  dataListaClientes: any;
  clientes: Cliente[];


  //Agregar Ventas | Lista productos
  listaDetalleVentaProducto: ListaDetalleVentaProducto[]
  popupVisible = false;
  //Boton EditarVenta PopUp
  buttonOptions: any = {
    text: "Guardar",
    type: "success",
    useSubmitBehavior: true
  }

  constructor(service: Service, serviceUp: ServiceUp, private productoService: ProductoService,
    serviceDGE: ServiceDGE, private ventaService: VentaService, clienteService: ClienteService) {

    clienteService.getClientes()
      .then((r: Cliente[]) => {
        this.clientes = r;
        this.dataListaClientes = new ArrayStore({
          data: this.clientes,
          key: "id"
        });
      });

    //DataGridModule ventas
    ventaService.obtenerListaVentas()
      .then((r: VentaLista[]) => {
        this.dataSourceVentas = r;
      });

    //Select Box Agrupado
    this.productoService.getProductos()
      .then((res: ProductoLista[]) => {
        this.productosLista = res;
        this.listaProductos = new DataSource({
          store: new ArrayStore({
            data: this.productosLista,
            key: "id"
          }),
          map: function (item) {
            item.key = item.nombre;
            item.items = item.listaExistenciaDetalleProductos;
            console.log(item);
            return item;
          }
        });
      });
  }


  ngOnInit() { }



  onValueChanged(e) {
    const previousValue = e.previousValue;
    const newValue = e.value;
    var datosEncontrados = this.productosLista
      .find(e => e.listaExistenciaDetalleProductos.find(l => l.id == newValue)).id
    // .filter((f) => {
    //   var filtroEncontrado = f.listaExistenciaDetalleProductos.filter(f => f.id == e.value);
    //   return filtroEncontrado.keys[0]
    //   })
    console.log(e.value + " : " + datosEncontrados);
    var editarDetalleProducto = new EditarDetalleVentaProducto(e.value, datosEncontrados)
    this.ventaService.editarVentaDetallePromise(editarDetalleProducto, this.selectVentaLista.id,
      e.previousValue.id)
      .then(() => {
        notify({
          message: "Actualizacion exitosa",
          position: {
            my: "center top",
            at: "center top"
          }
        }, "success", 3000);
      });
  }

  getPhonesOptions(phones: any) {

    let options = [];
    for (let i = 0; i < phones.length; i++) {
      options.push((i));
      console.log(i);
    }

    return options;
  }
  logEvent(eventName) {
    console.log(eventName);
  }


  @ViewChild('dataGridRef') dataGrid: DxDataGridComponent;

  selectedRowsData = [];
  getSelectedData() {
    this.selectedRowsData = this.dataGrid.instance.getSelectedRowsData();

    // ===== or when deferred selection is used =====
    this.dataGrid.instance.getSelectedRowsData().then((selectedRowsData) => {
      // Your code goes here
    });
  }


  editRowKey: number;

  onEditingStart(e) {
    this.editRowKey = e.key;
    this.selectVentaLista = e.data;
    this.selectVentaListaProducto = this.selectVentaLista.listaDetalleVentaProducto;
    this.popupVisible = true;
    // ...
  }


  onFormSubmit = function (e) {
    console.log(this.selectVentaLista);

    var editarVenta: EditarVenta = new EditarVenta(this.selectVentaLista.fehcaVenta, this.selectVentaLista.clienteId);
    this.guardarCambio(editarVenta, this.selectVentaLista.id)
    e.preventDefault();
  }

  guardarCambio(editarVenta, id) {
    this.ventaService.editarVentaPromise(editarVenta, id).then(() => {
      notify({
        message: "Actualizacion exitosa",
        position: {
          my: "center top",
          at: "center top"
        }
      }, "success", 3000);
      this.popupVisible = false;
    });
  }

  popupCrearVentaActivar(e) {
    this.popupCrearVenta = true;
  }


  onCrearVenta(e) {
    console.log(this.crearVenta);

    this.popupCrearVenta = true;
    e.preventDefault();
    this.ventaService.crearVentaPromise(this.crearVenta)
      .then((r: VentaLista) => {
        let ventaId = r.id;
        this.crearVentaListaProducto.forEach(element => {

          this.ventaService.crearVentaDetallePromise(element, ventaId)
            .then((r) => {
              console.log(r);
            });
        });
        notify({
          message: "Creacion exitosa",
          position: {
            my: "center top",
            at: "center top"
          }
        }, "success", 3000);

        this.popupCrearVenta = false;
        this.crearVentaListaProducto = [];
        this.crearVenta = new CrearVenta("", 0, new Cliente());


        let dataGridDataSource = this.dataGrid.instance.getDataSource();
        dataGridDataSource.reload();

      });
  }

  onSelectCliente(e) {
    const newValue = e.value;
    var datosEncontrados = this.clientes
      .find(l => l.id == newValue);


    this.crearVenta.clienteId = datosEncontrados.id;
    this.crearVenta.cliente.nombre = datosEncontrados.nombre;
    this.crearVenta.cliente.apellido = datosEncontrados.apellido;

    console.log(datosEncontrados);
  }

  agregarProducto(e) {
    const previousValue = e.previousValue;
    const newValue = e.value;
    var datosEncontrados = this.productosLista
      .find(e => e.listaExistenciaDetalleProductos.find(l => l.id == newValue)).id

    var elemeto = new ListaDetalleVentaProducto()
    elemeto.productoId = datosEncontrados;
    elemeto.existenciaProductoId = e.value;

    this.crearVentaListaProducto.push(elemeto);

  }

}

 


