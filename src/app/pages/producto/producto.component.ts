import { Component, OnInit } from '@angular/core';
import { ProductoLista, ProductosLista } from 'src/app/shared/models/producto';
import { ProductoService } from 'src/app/shared/services/producto/producto.service';

@Component({
  selector: 'app-producto',
  templateUrl: './producto.component.html',
  styleUrls: ['./producto.component.scss'],
  providers: [ProductoService]

})
export class ProductoComponent implements OnInit {
  //DataGridModule Clientes
  dataSourceProductoLista: ProductosLista[]; 

  constructor(private productoService: ProductoService) {

    productoService.getProductos()
      .then((r: ProductosLista[]) => {
        this.dataSourceProductoLista = r;
        console.log(this.dataSourceProductoLista);
      });

  } 

  ngOnInit() {
  }

}
