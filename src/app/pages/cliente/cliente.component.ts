import { Component, OnInit, ViewChild } from '@angular/core';

import ArrayStore from 'devextreme/data/array_store';
import { Product, Service  } from 'src/app/shared/services/pruebaSelect/product.service';
import {  ProductUp, ServiceUp } from 'src/app/shared/services/pruebaSelect/productUp.service';

import DataSource from 'devextreme/data/data_source';
import { ProductoService } from 'src/app/shared/services/producto/producto.service';
import { ProductoLista } from 'src/app/shared/models/producto';
import { Input } from '@angular/core';
import { Employee, ServiceDGE, State } from 'src/app/shared/services/pruebaSelect/dataGridModule';
import { VentaService } from 'src/app/shared/services/ventas/venta.service';
import { CrearVenta, EditarDetalleVentaProducto, EditarVenta, ListaDetalleVentaProducto, VentaLista } from 'src/app/shared/models/venta';
import { DxDataGridComponent } from 'devextreme-angular';
import notify from 'devextreme/ui/notify';
import { ClienteService } from 'src/app/shared/services/cliente/cliente.service';
import { Cliente } from 'src/app/shared/models/cliente';


@Component({
  selector: 'app-cliente',
  templateUrl: './cliente.component.html',
  styleUrls: ['./cliente.component.scss'],
  providers: [Service, ServiceUp, ServiceDGE, ProductoService, VentaService, ClienteService ]
})

export class ClienteComponent implements OnInit {

  // constructor() { }  
  
  //Select Box Agrupado
  listaProductos: DataSource;
  productosLista: ProductoLista[]; 
 

 

  //DataGridModule Clientes
  dataSourceClientes: Cliente[]; 

  constructor(service: Service, serviceUp: ServiceUp, private productoService: ProductoService, 
    serviceDGE: ServiceDGE, private ventaService: VentaService, clienteService:ClienteService) {
    
    clienteService.getClientes()
      .then((r:Cliente[])=>{
        this.dataSourceClientes = r;
    });   
 
  } 
  ngOnInit() { } 
 
}
