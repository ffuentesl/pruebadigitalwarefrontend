export const navigation = [
  {
    text: 'Home',
    path: '/home',
    icon: 'home'
  },
  {
    text: 'Examples',
    icon: 'folder',
    items: [
      {
        text: 'Profile',
        path: '/profile'
      },
      {
        text: 'Tasks',
        path: '/tasks'
      }
    ]
  },
  {
    text: 'Cliente',
    path: '/pages/cliente',
    icon: 'card'
  },
  {
    text: 'Venta',
    path: '/pages/venta',
    icon: 'money'
  },
  {
    text: 'Producto',
    path: '/pages/producto',
    icon: 'box'
  }
];
