import { NgModule, Component, ViewChild, enableProdMode, AfterViewInit } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { HttpClientModule } from '@angular/common/http';
 
import { AppComponent } from './app.component';
import { SideNavOuterToolbarModule, SideNavInnerToolbarModule, SingleCardModule } from './layouts';
import { FooterModule, ResetPasswordFormModule, CreateAccountFormModule, ChangePasswordFormModule, LoginFormModule } from './shared/components';
import { AuthService, ScreenService, AppInfoService } from './shared/services';
import { UnauthenticatedContentModule } from './unauthenticated-content';
import { AppRoutingModule } from './app-routing.module';
import { DxSelectBoxModule, DxTextBoxModule,DxTemplateModule,DxDataGridModule, DxTextAreaModule, DxFormModule, DxButtonModule, DxPopupModule, DxScrollViewModule } from 'devextreme-angular';
 

import { VentaService } from 'src/app/shared/services/ventas/venta.service';
import { Product, Service } from './shared/services/pruebaSelect/product.service';



@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    DxSelectBoxModule,
    DxTextBoxModule,
    DxTemplateModule,
    FormsModule, 
    SideNavOuterToolbarModule,
    SideNavInnerToolbarModule,
    SingleCardModule,
    FooterModule,
    ResetPasswordFormModule,
    CreateAccountFormModule,
    ChangePasswordFormModule,
    LoginFormModule,
    UnauthenticatedContentModule,
    AppRoutingModule,
    DxDataGridModule,
    HttpClientModule, 
    DxTextAreaModule,
    DxFormModule,
    DxButtonModule,
    DxPopupModule,
    DxButtonModule,
    DxTemplateModule, DxScrollViewModule
  ],
  providers: [
    AuthService,
     ScreenService,
     AppInfoService,
     VentaService,
    Product, Service
    ],
  bootstrap: [AppComponent]
})

export class AppModule { }
platformBrowserDynamic().bootstrapModule(AppModule);


